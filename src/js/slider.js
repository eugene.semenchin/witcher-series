var swiper = new Swiper(".swiper", {
    slidesPerView: 1,
    spaceBetween: 16,
    loop: false,
    freeMode: true,
    autoplay: false,
    speed: 1000,
    breakpoints: {
        425: {
            slidesPerView: 2,
            spaceBetween: 24,
        },

        769: {
            slidesPerView: 3,
            spaceBetween: 24,
        },

        1281: {
            slidesPerView: 4,
        }
    },

    pagination: {
        el: ".swiper-pagination",
    },

    navigation: {
        prevEl: '.slide-prev',
        nextEl: '.slide-next',
      },
});