const moreBtn = document.querySelector('.more-btn');
const moreGallery = document.querySelector('.more-gallery');

moreBtn.addEventListener('click', () => {
    moreBtn.classList.add('disable');
    moreGallery.classList.add('active');
})

